import { Rate, Counter } from 'k6/metrics';
import getPagedata from './getPagedata.test.js';

let errorRate = new Rate('errorRate');
let errorCount = new Counter('errors');

export const options = {
    vus: 10,
    iterations: 100,
    thresholds: {
        errorRate: ["rate<0.1"], //10%
        errors: ["rate<5"]
    }
};

export default () => {
    getPagedata();
}