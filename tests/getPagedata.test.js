import { check, group } from 'k6'
import http from 'k6/http';

const host = 'https://api.ranker-stage.com'

export default () => {

    const list = '1678578'
    const path = `/lists/${list}/pagedata`

    return group('GET Pagedata lists', function() {
        const res = http
        .get(`${host}${path}`);

        let success = check(res, {
            'GET Lists Pagedata (Status 200)': (r) => r.status == 200
        });

        if(!success) {
            errorCount.add(1);
            errorRate(true);
        } else {
            errorRate.add(false);
        }

        return res;
     });
}

